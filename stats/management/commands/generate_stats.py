import datetime
import random

from db.utils import get_object_or_none
from django.core.management.base import BaseCommand
from profiles.models import User
from stats.models import WeeklyStat


class Command(BaseCommand):
    help = "Get the default initial user & generate random associated stats"

    def handle(self, *args, **options):
        """
        Gets the admin user defined by the make createsuperuser command

        Finds the next sunday date

        Generate random weekly stats per week for 20 weeks prior to next sunday
        """
        default_user = get_object_or_none(User, email='admin@email.com')
        weekly_stats = []

        d = datetime.date.today()

        while d.weekday() != 6:
            d += datetime.timedelta(1)

        if not default_user:
            print('Please run `make createsuperuser` command')
            return

        for x in range(20):
            pre_starts = random.randint(50, 200)
            comparisons = random.randint(10, pre_starts)
            weekly_stats.append(
                WeeklyStat(
                    agent=default_user,
                    week=d,
                    pre_starts=pre_starts,
                    comparisons=random.randint(10, pre_starts),
                    prospects=random.randint(0, comparisons)
                )
            )
            d -= datetime.timedelta(7)

        WeeklyStat.objects.bulk_create(weekly_stats)

        print(f'Weekly stats generated for user {default_user.email}')
