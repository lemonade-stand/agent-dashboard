from db.models import BaseModel
from django.db import models


class WeeklyStat(BaseModel):
    """
    Weekly Stat model to represent conversions associated with an agent for a single week
    """

    agent = models.ForeignKey(
        'profiles.User',
        on_delete=models.CASCADE,
        help_text='Associated agent for weekly metric.',
        verbose_name='Agent ID'
    )

    week = models.DateField(
        blank=False,
        null=True,
        default=None,
        help_text='First day of the week associated with the metric',
        verbose_name='First day of week'
    )

    pre_starts = models.PositiveBigIntegerField(
        default=None,
        blank=True,
        null=True,
        help_text='Number of pre-starts',
        verbose_name='Number of pre-starts'
    )

    comparisons = models.PositiveBigIntegerField(
        default=None,
        blank=True,
        null=True,
        help_text='Number of comparisons',
        verbose_name='Number of comparisons'
    )

    prospects = models.PositiveBigIntegerField(
        default=None,
        blank=True,
        null=True,
        help_text='Number of prospects',
        verbose_name='Number of prospects'
    )
