from rest_framework import authentication, permissions
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import WeeklyStat
import datetime
from .serializers import WeeklyStatSerializer
from django.db.models import Sum


class WeeklyStatViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows weekly stats to be viewed.
    """
    queryset = WeeklyStat.objects.all().order_by('-created_on')
    serializer_class = WeeklyStatSerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.SessionAuthentication]


class AggregatedStatsView(APIView):
    """
    View to aggregate Weekly stats for a user
    """
    authentication_classes = [authentication.SessionAuthentication]
    permission_classes = [permissions.IsAdminUser]

    def get(self, request, format=None):
        """
        Return aggregated weekly stats for a given timeframe
        """

        user = request.user
        start = self.request.query_params.get('start')
        end = self.request.query_params.get('end')

        if not start or not end:
            return Response(
                data=dict(error='Please enter a start and end date in `YYYY-MM-DD` format.'),
                status=400
            )

        try:
            start_date = datetime.datetime.strptime(start, '%Y-%m-%d')
        except ValueError:
            return Response(
                data=dict(error='Invalid `start` parameter.  Please input dates in `YYYY-MM-DD` format.'),
                status=400
            )
        try:
            end_date = datetime.datetime.strptime(end, '%Y-%m-%d')
        except ValueError:
            return Response(
                data=dict(error='Invalid `end` parameter.  Please input dates in `YYYY-MM-DD` format.'),
                status=400
            )

        stats = WeeklyStat.objects.filter(
            agent=user,
            week__gte=start_date,
            week__lte=end_date
        )

        pre_starts = stats.aggregate(Sum('pre_starts')).get('pre_starts__sum')
        comparisons = stats.aggregate(Sum('comparisons')).get('comparisons__sum')
        prospects = stats.aggregate(Sum('prospects')).get('prospects__sum')

        return Response(
            dict(
                pre_starts=pre_starts,
                comparisons=comparisons,
                prospects=prospects
            )
        )
