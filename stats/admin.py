from django.contrib import admin

from .models import WeeklyStat


class WeeklyStatAdmin(admin.ModelAdmin):
    list_display = (
        'agent',
        'week',
        'pre_starts',
        'comparisons',
        'prospects',
    )


admin.site.register(WeeklyStat, WeeklyStatAdmin)
