from .models import WeeklyStat
from rest_framework import serializers


class WeeklyStatSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WeeklyStat
        fields = '__all__'
