# README #

### What is this repository for? ###

* Django repository for the Agent Dashboard API exercise.

### How do I get set up? ###

This project is setup with Django / Django Rest Framework & SQLite

To get started:

- Clone Repository : `git clone https://bitbucket.org/lemonade-stand/agent-dashboard.git`
- Navigate to repository directory : `cd agent-dashboard/`
- Create virtual environment : `make make-env`
- Activate Virtual environment : `source env/bin/activate`
- Copy env file locally : `cp .example-env api/.env`
- Run migrations : `make migrate`
- Create Superuser: `make createsuperuser`
- Seed database with weekly statistics: `make generate-stats`
- Runserver : `make runserver`

This will run the django project at `localhost:8000`

### Steps to test:

(With project running)

- Navigate to django admin & login as demo superuser
  - Username `testuser`
  - Password: `testpass`
- View aggregated stats for the user : `http://localhost:8000/aggregated-stats/?start=2021-01-01&end=2021-10-31`
- Toggle query parameters to view changes in response

You should see a response with the following format :

Status Code `200`
```json
  {
    "pre_starts": 290,
    "comparisons": 85,
    "prospects": 60
  }
```

Without query parameters you should receive :

status Code: `400`
```json
  {
    "error": "Please enter a start and end date in `YYYY-MM-DD` format."
  }
```

Bad `start` query parameters should yield :

status Code: `400`
```json
  {
    "error": "Invalid `start` parameter.  Please input dates in `YYYY-MM-DD` format."
  }
```

### Potential Followup Tasks:

- Configure with active database
- Add unit tests for :
    - Registering a user
    - Viewing GET endpoints
    - Permissions for accessing stats
    - Accuracy of stats
- Utilize docker to run project
- Deploy to hosting solution (AWS / GCP / etc)
- Add deployments via bitbucket-pipelines