from django.contrib.auth.models import AbstractUser
from db.models import BaseModel
from django.db import models


class User(AbstractUser, BaseModel):

    is_agent = models.BooleanField(
        default=False
    )
