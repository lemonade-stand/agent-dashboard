from django.contrib import admin
from django.urls import include, path
from profiles.views import UserViewSet
from rest_framework import routers
from stats.views import WeeklyStatViewSet, AggregatedStatsView

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'weekly-stats', WeeklyStatViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('aggregated-stats/', AggregatedStatsView.as_view(), name='aggregated-stats'),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
