.DEFAULT_GOAL := help

make-env:
	-python3 -m venv env

start-env:
	-source env/bin/activate

install-requirements:
	-pip3 install -r requirements.txt

start:
	-python3 manage.py runserver

start:
	-python3 manage.py runserver

migrate:
	-python3 manage.py migrate

generate-stats:
	-python3 manage.py generate_stats

createsuperuser:
	-DJANGO_SUPERUSER_PASSWORD=testpass python manage.py createsuperuser --username testuser --email admin@email.com --noinput

#############################################################
# "Help Documentation"
#############################################################

help:
	@echo "\033[Agent Dashboard Project Commands\033[0m"
	@echo " - Run Development Server:"
	@echo "    make start"
	@echo " - Run Migrations Locally:"
	@echo "    make migrate"
	@echo " - Make Migrations Locally:"
	@echo "    make makemigrations"
	@echo " - Create Super User Locally:"
	@echo "    make createsuperuser"

.PHONY:
	start
	make-env
	start-env
	install-requirements
	createsuperuser